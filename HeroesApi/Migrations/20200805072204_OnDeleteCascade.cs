﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace HeroesApi.Migrations
{
    public partial class OnDeleteCascade : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_Villain_Id_hero",
                table: "Villain",
                column: "Id_hero");

            migrationBuilder.AddForeignKey(
                name: "FK_Villain_Hero_Id_hero",
                table: "Villain",
                column: "Id_hero",
                principalTable: "Hero",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Villain_Hero_Id_hero",
                table: "Villain");

            migrationBuilder.DropIndex(
                name: "IX_Villain_Id_hero",
                table: "Villain");
        }
    }
}
