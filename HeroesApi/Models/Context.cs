﻿using Microsoft.EntityFrameworkCore;

namespace HeroesApi
{
    public class Context : DbContext
    {
        public Context() { }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer($@"Server=.\SQLEXPRESS;Database=TourOfHeroes;Trusted_Connection=True;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Villain>()
                .HasOne<Hero>(villain => villain.Hero)
                .WithMany(hero => hero.Villains)
                .HasForeignKey(villain => villain.Id_hero);
        }

        public DbSet<Hero> Heroes { get; set; }
        public DbSet<Villain> Villains { get; set; }
    }
}
