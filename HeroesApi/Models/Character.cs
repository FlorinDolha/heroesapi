﻿using System.ComponentModel.DataAnnotations;

namespace HeroesApi
{
    public class Character
    {
        [Key]
        public int Id { get; set; }

        [StringLength(30)]
        [Required]
        public string Name { get; set; }
    }
}
