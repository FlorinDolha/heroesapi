﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace HeroesApi
{
    [Table("Hero")]
    public class Hero : Character
    {
        [NotMapped]
        public ICollection<Villain> Villains { get; set; }
    }
}
