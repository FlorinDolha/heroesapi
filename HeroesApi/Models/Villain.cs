﻿using System.ComponentModel.DataAnnotations.Schema;

namespace HeroesApi
{
    [Table("Villain")]
    public class Villain : Character
    {
        [ForeignKey("Hero")]
        public int Id_hero { get; set; }

        [NotMapped]
        public Hero Hero { get; set; }
    }
}
