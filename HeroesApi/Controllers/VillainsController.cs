﻿using HeroesApi;
using HeroesApi.HubConfig;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using System.Collections.Generic;
using System.Linq;

namespace VillainsApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VillainsController : ControllerBase
    {
        public Context Context { get; }
        private IHubContext<VillainHub> _hub;

        public VillainsController(IHubContext<VillainHub> hub)
        {
            Context = new Context();
            _hub = hub;
        }

        [HttpGet]
        public IEnumerable<Villain> Get([FromQuery] string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                return Context.Villains;
            }

            return Context.Villains.Where(villain => villain.Name.ToLower().Contains(name.ToLower()))
                                   .ToArray();
        }

        [HttpGet("id_hero/{id_hero}")]
        public IEnumerable<Villain> GetByHero(int id_hero)
        {
            return Context.Villains.Where(villain => villain.Id_hero == id_hero);
        }

        [HttpGet("{id}")]
        public Villain Get(int id)
        {
            return Context.Villains.FirstOrDefault(Villain => Villain.Id == id);
        }

        [HttpPost]
        public Villain Post([FromBody] Villain villain)
        {
            Context.Villains.Add(villain);
            Context.SaveChanges();

            _hub.Clients.All.SendAsync("villainsChanged", Context.Villains);

            return villain;
        }

        [HttpPut]
        public Villain[] Put([FromBody] Villain villain)
        {
            Villain oldVillain = Context.Villains.Find(villain.Id);
            oldVillain.Name = villain.Name;
            Context.Update(oldVillain);
            Context.SaveChanges();

            _hub.Clients.All.SendAsync("villainsChanged", Context.Villains);

            return Context.Villains.ToArray();
        }

        [HttpDelete("{id}")]
        public Villain[] Delete(int id)
        {
            Villain villain = Context.Villains.Find(id);

            if (villain == null)
            {
                return null;
            }

            Context.Villains.Remove(villain);
            Context.SaveChanges();

            _hub.Clients.All.SendAsync("villainsChanged", Context.Villains);

            return Context.Villains.ToArray();
        }
    }
}
