﻿using HeroesApi.HubConfig;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using System.Collections.Generic;
using System.Linq;

namespace HeroesApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class HeroesController : ControllerBase
    {
        public Context Context { get; }
        private readonly IHubContext<HeroHub> _hub;

        public HeroesController(IHubContext<HeroHub> hub)
        {
            Context = new Context();
            _hub = hub;
        }

        [HttpGet]
        public IEnumerable<Hero> Get([FromQuery] string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                return Context.Heroes;
            }

            return Context.Heroes.Where(hero => hero.Name.ToLower().Contains(name.ToLower()));
        }

        [HttpGet("{id}")]
        public Hero Get(int id)
        {
            return Context.Heroes.FirstOrDefault(Hero => Hero.Id == id);
        }

        [HttpPost]
        public Hero Post([FromBody] Hero hero)
        {
            Context.Heroes.Add(hero);
            Context.SaveChanges();

            _hub.Clients.All.SendAsync("heroesChanged", Context.Heroes);

            return hero;
        }

        [HttpPut]
        public Hero[] Put([FromBody] Hero hero)
        {
            Hero oldHero = Context.Heroes.Find(hero.Id);
            oldHero.Name = hero.Name;
            Context.Update(oldHero);
            Context.SaveChanges();

            _hub.Clients.All.SendAsync("heroesChanged", Context.Heroes);

            return Context.Heroes.ToArray();
        }

        [HttpDelete("{id}")]
        public Hero[] Delete(int id)
        {
            Hero hero = Context.Heroes.Find(id);

            if (hero == null)
            {
                return null;
            }

            Context.Heroes.Remove(hero);
            Context.SaveChanges();

            _hub.Clients.All.SendAsync("heroesChanged", Context.Heroes);

            return Context.Heroes.ToArray();
        }

        //public IActionResult Get()
        //{
        //    _hub.Clients.All.SendAsync("heroesChanged", Context.Heroes);

        //    return Ok(new { Message = "Request Complete" });
        //}
    }
}
